// utility routine declarations

extern int Roll(int);
extern char *A_Or_An(const char *);
extern char const *Choose(char const *Words[], int Number);
extern int License_Seed(void);
extern int CountStrings(const char *[]);

extern void PutMisc(void);
extern void PutThanks(void);
extern void PutName(void);
extern void PutJob(void);
extern void PutAge(void);
extern void PutLicense(void);
extern void PutTemperature(void);
extern void PutWeather1(void);
extern void PutWeather2(void);
extern void PutWeather(void);
extern void PutCityState(void);
extern void PutLocation(void);
extern void PutRig(void);
extern void PutRST(void);
extern void PutQ_And_Freq(void);
extern void PutFirstCallsign(void);
extern void PutLastCallsign(void);

extern void PutQSO(void);

extern int make_freq(void);

// end
